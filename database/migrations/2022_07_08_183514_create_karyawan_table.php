<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->timestamps();
            $table->string('nm_karyawan');
            $table->date('tgl_lahir');
            $table->string('tempat_lahir', 50);
            $table->integer('gaji_pokok');
            $table->boolean('jenis_kelamin');
            $table->unsignedBigInteger('jabatan_id');

            $table->foreign('jabatan_id')->references('id')->on('jabatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
