@extends('layouts.app2')

{{-- @section('content')
    <style>
        .card-header {
            text-align: center;
        }

        section {
            height: 100vh;
            width: 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-size: 50%;
            background-attachment: fixed;
        }

        .image-1 {
            background-image: url("assets/milkyway.jpg")
        }

        .image-2 {
            background-image: url("assets/nebula.jpg");
        }

        .image-3 {
            background-image: url("assets/nebula1.jpg");
        }

        .image-4 {
            background-image: url("assets/universe.jpg");
        }
    </style>

    <body class="body">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">

                    <div class="card">
                        <div class="card-header">{{ __('My Portofolio') }}</div>
                        <div class="w3-row-padding">
                            <div class="w3-container">
                                <section class="section">
                                    <h4>Preview</h4>
                                    <section class="image-1"></section>
                                    <section class="image-2"></section>
                                    <section class="image-3"></section>
                                    <section class="image-4"></section>
                                </section>
                            </div>
                            @foreach ($portofolios as $portofolio)
                                <div class="w3-third w3-container w3-margin-bottom">
                                    <img src="{{ asset($portofolio->image) }}" alt="Norway" style="width:100%"
                                        class="w3-hover-opacity">
                                    <div class="w3-container w3-white">
                                        <p><b>{{ $portofolio->title }}</b></p>
                                        <p>{{ $portofolio->description }}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </body>
@endsection --}}
@section('content')
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <style>
        body,
        h1,
        h2,
        h3,
        h4,
        h5 {
            font-family: "Raleway", sans-serif
        }

        .w3-quarter img {
            margin-bottom: -6px;
            cursor: pointer
        }

        .w3-quarter img:hover {
            opacity: 0.6;
            transition: 0.3s transform: scaleX(-1);
        }

        img.me {}
    </style>
    </head>

    <body class="w3-light-grey">

        <!-- Sidebar/menu -->
        <nav class="w3-sidebar w3-bar-block w3-black w3-animate-right w3-top w3-text-light-grey w3-large"
            style="z-index:3;width:250px;font-weight:bold;display:none;right:0;" id="mySidebar">
            <a href="javascript:void()" onclick="w3_close()" class="w3-bar-item w3-button w3-center w3-padding-32">Close</a>
            <a href="#" onclick="w3_close()" class="w3-bar-item w3-button w3-center w3-padding-16">Portofolio</a>
            <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button w3-center w3-padding-16">About Me</a>
            <a href="#contact" onclick="w3_close()" class="w3-bar-item w3-button w3-center w3-padding-16">Contact</a>
            <a href="#price" onclick="w3_close()" class="w3-bar-item w3-button w3-center w3-padding-16">My Price</a>
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();"
                class="w3-bar-item w3-button w3-center w3-padding-16">Log Out</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </nav>

        <!-- Top menu on small screens -->
        <header class="w3-container w3-top w3-white w3-xlarge w3-padding-16">
            <span id="time"></span>
            <a href="javascript:void(0)" class="w3-right w3-button w3-white" onclick="w3_open()">Menu</a>
        </header>

        <!-- Overlay effect when opening sidebar on small screens -->
        <div class="w3-overlay w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu"
            id="myOverlay"></div>

        <!-- !PAGE CONTENT! -->
        <div class="w3-main w3-content" style="max-width:1600px;margin-top:83px">

            <!-- Photo grid -->
            <div class="w3-row w3-grayscale-min">
                <div class="w3-quarter">
                    <img src="assets/1.jpg" style="width:100%" onclick="onClick(this)" alt="Black Hole">
                    <img src="assets/2.jpg" style="width:100%" onclick="onClick(this)" alt="hahahaha">
                    <img src="assets/3.jpg" style="width:100%" onclick="onClick(this)" alt="Beautiful, isn't it?">
                </div>

                <div class="w3-quarter">
                    <img src="assets/4.jpg" style="width:100%" onclick="onClick(this)" alt="Intersteller Ending">
                    <img src="assets/universe.jpg" style="width:100%" onclick="onClick(this)" alt="Are we alone?">
                    <img src="assets/5.jpg" style="width:100%" onclick="onClick(this)"
                        alt="No particles or even electromagnetic radiation such as light can escape from it.">
                </div>

                <div class="w3-quarter">
                    <img src="assets/6.jpg" style="width:100%" onclick="onClick(this)"
                        alt="Where do we come from? Why are we here?">
                    <img src="assets/7.jpg" style="width:100%" onclick="onClick(this)"
                        alt="we're just a particle in this universes">
                    <img src="assets/8.jpg" style="width:100%" onclick="onClick(this)" alt="Beautiful isn't it">
                </div>

                <div class="w3-quarter">
                    <img src="assets/milkyway.jpg" style="width:100%" onclick="onClick(this)" alt="Silence">
                    <img src="assets/nebula1.jpg" style="width:100%" onclick="onClick(this)" alt="Space">
                    <img src="assets/nebula2.jpg" style="width:100%" onclick="onClick(this)" alt="Outer Space">
                </div>
            </div>

            <!-- Pagination -->
            <div class="w3-center w3-padding-32">
                <div class="w3-bar">
                    <a href="#" class="w3-bar-item w3-button w3-hover-black">«</a>
                    <a href="#" class="w3-bar-item w3-black w3-button">1</a>
                    <a href="#" class="w3-bar-item w3-button w3-hover-black">2</a>
                    <a href="#" class="w3-bar-item w3-button w3-hover-black">3</a>
                    <a href="#" class="w3-bar-item w3-button w3-hover-black">4</a>
                    <a href="#" class="w3-bar-item w3-button w3-hover-black">»</a>
                </div>
            </div>

            <!-- Modal for full size images on click-->
            <div id="modal01" class="w3-modal w3-black" style="padding-top:0" onclick="this.style.display='none'">
                <span class="w3-button w3-black w3-xlarge w3-display-topright">×</span>
                <div class="w3-modal-content w3-animate-zoom w3-center w3-transparent w3-padding-64">
                    <img id="img01" class="w3-image">
                    <p id="caption"></p>
                </div>
            </div>

            <!-- About section -->
            <div class="w3-container w3-dark-grey w3-center w3-text-light-grey w3-padding-32" id="about">
                <h4><b>About Me</b></h4>
                <img src="assets/4.jpg" alt="Me" class="w3-image w3-padding-10" width="600" height="650">
                <div class="w3-content w3-justify" style="max-width:600px">
                    <h4>Saul P. Liunokas</h4>
                    <p>Kita adalah kepingan kecil dari bagian-bagian Alam Semesta yang tahun ke tahun ukuran nya semakin
                        membesar.
                        Apakah anda pernah terpikirkan mengenai keadaan alam semesta tetang apa yang terjadi sebelum <a
                            href="https://en.wikipedia.org/wiki/The_Big_Bang_Theory"><i>Big Bang</i></a>?
                        Apakah anda pernah terpikirkan mengenai konsep <i>White Hole</i>? Karena memang yang selama selalu
                        dipublikasikan hanya <a href="https://en.wikipedia.org/wiki/Black_hole"><i>Black Hole</i></a>
                        masih banyak lagi teori-teori yang memang belum terpecahkan di dunia ini. Dan karena itulah saya
                        mengumpulkan berbagai macam gambar mengenai luar angkasa di Portofolio ini.
                    </p>
                    <p><a href="mailto: saul.liunokas.com">Email : saul.liunokas@gmail.com</a>
                    <p>
                    <p><a href="https://wa.me/62081385907992/">Text Me : +6281385907992</a></p>
                    <hr class="w3-opacity">
                    <h4 class="w3-padding-16">Skills</h4>
                    <p class="w3-wide">Photography</p>
                    <div class="w3-white">
                        <div class="w3-container w3-padding-small w3-center w3-grey" style="width:95%">95%</div>
                    </div>
                    <p class="w3-wide">Web Design</p>
                    <div class="w3-white">
                        <div class="w3-container w3-padding-small w3-center w3-grey" style="width:85%">85%</div>
                    </div>
                    <p class="w3-wide">Back End</p>
                    <div class="w3-white">
                        <div class="w3-container w3-padding-small w3-center w3-grey" style="width:80%">80%</div>
                    </div>
                    <p><button
                            class="w3-button w3-light-grey w3-padding-large w3-margin-top w3-margin-bottom w3-hover-black">Download
                            Resume</button></p>
                    <hr class="w3-opacity">


                    <h4 class="w3-padding-16">Biaya Photograph</h4>
                    <div class="w3-row-padding" style="margin:0 -16px" id="price">
                        <div class="w3-half w3-margin-bottom">
                            <ul class="w3-ul w3-white w3-center">
                                <li class="w3-black w3-xlarge w3-padding-32">Basic</li>
                                <li class="w3-padding-16">Web Design</li>
                                <li class="w3-padding-16">Photography</li>
                                <li class="w3-padding-16">Bingkai Foto</li>
                                <li class="w3-padding-16">5GB Storage</li>
                                <li class="w3-padding-16">Edit Support</li>
                                <li class="w3-padding-16">
                                    <h2>$ 10</h2>
                                    <span class="w3-opacity">per bulan</span>
                                </li>
                                <li class="w3-light-grey w3-padding-24">
                                    <button class="w3-button w3-white w3-padding-large">Daftar?</button>
                                </li>
                            </ul>
                        </div>

                        <div class="w3-half">
                            <ul class="w3-ul w3-white w3-center">
                                <li class="w3-black w3-xlarge w3-padding-32">Pro</li>
                                <li class="w3-padding-16">Web Design</li>
                                <li class="w3-padding-16">Photography</li>
                                <li class="w3-padding-16">Bingkai Foto</li>
                                <li class="w3-padding-16">50GB Storage</li>
                                <li class="w3-padding-16">Endless Support</li>
                                <li class="w3-padding-16">
                                    <h2>$ 25</h2>
                                    <span class="w3-opacity">per bulan</span>
                                </li>
                                <li class="w3-light-grey w3-padding-24">
                                    <button class="w3-button w3-white w3-padding-large">Daftar?</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Contact section -->
            <div class="w3-container w3-light-grey w3-padding-32 w3-padding-large" id="contact">
                <div class="w3-content" style="max-width:600px">
                    <h4 class="w3-center"><b>Contact Me</b></h4>
                    <p>Do you want me to photograph you? Fill out the form and fill me in with the details :) I love meeting
                        new people!</p>
                    {{-- <form action="/welcome_get.php" method="GET"> --}}
                    {{-- <form id="demo" method="GET"> --}}
                    <div id="demo">
                        <div class="w3-section">
                            <label>Name</label>
                            <input class="w3-input w3-border" id="nama" type="text" name="name" required>
                        </div>
                        <div class="w3-section">
                            <label>Email</label>
                            <input class="w3-input w3-border" id="email" type="text" name="email" required>
                        </div>
                        <div class="w3-section">
                            <label>Message</label>
                            <input class="w3-input w3-border" id="message" type="text" name="message" required>
                        </div>
                        <button onclick="myFunction()" class="w3-button w3-block w3-black w3-margin-bottom">Send
                            Message</button>
                    </div>
                    {{-- </form> --}}
                </div>
            </div>

            <!-- Footer -->
            <footer class="w3-container w3-padding-32 w3-blue">
                <div class="w3-row-padding">
                    <div class="w3-third">
                        <h3>INFO</h3>
                        <p>My Name is Saul, third years of collage at STMIK Widuri Jakarta. looking formward to Photograph
                            your happy moment, my side project is photograph the sky, and the outer space</p>
                    </div>


                    <div class="w3-third">
                        <h3>BLOG POSTS</h3>
                        <ul class="w3-ul">
                            <li class="w3-padding-16 w3-hover-black">
                                <img src="/assets/blackhole.gif" class="w3-left w3-margin-right" style="width:50px">
                                <span class="w3-large">hahaha</span><br>
                                <span>i've been work with NASA before, about learning the Black Hole with General Relativity
                                    that Einstein has found before.</span>
                            </li>
                            <li class="w3-padding-16 w3-hover-black">
                                <img src="assets/stmik.png" class="w3-left w3-margin-right" style="width:50px">
                                <span class="w3-large">STMIK Widuri</span><br>
                                <span>STMIK Widuri mempunyai tanggung jawab
                                    untuk mencerdaskan manusia Indonesia dan mengupayakan perbaikan dan kemajuan masyarakat
                                    Indonesia</span>
                            </li>
                        </ul>
                    </div>

                    <div class="w3-third">
                        <h3>POPULAR TAGS</h3>
                        <p>
                            <span class="w3-tag w3-black w3-margin-bottom">Earth</span> <span
                                class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Nebula</span> <span
                                class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Black Hole</span>
                            <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">White Hole</span> <span
                                class="w3-tag w3-dark-grey w3-small w3-margin-bottom">MilkyWay</span> <span
                                class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Andromeda</span>
                            <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Constellation</span> <span
                                class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Sun</span><span
                                class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Moon</span>
                            <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Saturn</span> <span
                                class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Jupiter</span> <span
                                class="w3-tag w3-dark-grey w3-small w3-margin-bottom">UY Scuti</span>
                            <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Vega</span> <span
                                class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Event Horizon</span>
                        </p>
                    </div>
                </div>
            </footer>

            <div class="w3-black w3-center w3-padding-24">Powered by <a
                    href="https://en.wikipedia.org/wiki/Observable_universe" title="W3.CSS" target="_blank"
                    class="w3-hover-opacity">Observable Universe</a></div>

            <!-- End page content -->
        </div>

        <script>
            // Script to open and close sidebar
            function w3_open() {
                document.getElementById("mySidebar").style.display = "block";
                document.getElementById("myOverlay").style.display = "block";
            }

            function w3_close() {
                document.getElementById("mySidebar").style.display = "none";
                document.getElementById("myOverlay").style.display = "none";
            }

            // Modal Image Gallery
            function onClick(element) {
                document.getElementById("img01").src = element.src;
                document.getElementById("modal01").style.display = "block";
                var captionText = document.getElementById("caption");
                captionText.innerHTML = element.alt;
            }
            // Time
            function currentTime() {
                let date = new Date();
                let hh = date.getHours();
                let mm = date.getMinutes();
                let ss = date.getSeconds();
                let session = "AM";


                if (hh > 12) {
                    session = "PM";
                }

                hh = (hh < 10) ? "0" + hh : hh;
                mm = (mm < 10) ? "0" + mm : mm;
                ss = (ss < 10) ? "0" + ss : ss;

                let time = hh + ":" + mm + ":" + ss + " " + session;

                document.getElementById("time").innerText = time;
                let t = setTimeout(function() {
                    currentTime()
                }, 1000);

            }

            currentTime();

            function myFunction() {
                let text = "Press a button!\nEither OK or Cancel.";
                if (confirm(text) == true) {
                    var nama = document.getElementById("nama").value;
                    var email = document.getElementById("email").value;
                    var message = document.getElementById("message").value;
                    // = "You pressed OK!";
                } else {
                    text = "You canceled!";
                }
                document.getElementById("demo").innerHTML = "nama anda : " + nama + "<br>" + "email anda : " + email + "<br>" +
                    "pesan anda : " + message;
            }
        </script>
    </body>
@endsection
